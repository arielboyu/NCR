import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/login";
import Main from "./components/main";
import Detail from "./components/detail";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/menu" component={Main} />
          <Route exact path="/detalles/:id" component={Detail} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
