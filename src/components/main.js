import React, { Component } from "react";
import Cookies from "universal-cookie";

let prev = 0;
let next = 0;
let last = 0;

const cookies = new Cookies();

class Main extends Component {
  constructor() {
    super();
    this.state = {
      accounts: [""],
      currentPage: 1,
      allPerPage: 5,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: Number(event.target.id),
      allPerPage: 4,
    });
  }

  LogOut = () => {
    cookies.remove("idnum", { path: "/" });
    cookies.remove("nombre", { path: "/" });
    cookies.remove("username", { path: "/" });
    cookies.remove("accounts", { path: "/" });
    window.location.href = "./";
  };

  componentDidnumMount() {
    if (!cookies.get("username")) {
      window.location.href = "./";
    }
  }

  accounts = cookies.get("accounts");


  typeAccount(type) {
    if (type === "CC") {
      return "Cuenta Corriente";
    } else if (type === "CA") {
      return "Caja De Ahorro";
    } else if (type === "CCP") {
      return "Caja De Ahorro en Pesos ";
    }
  }

  render() {
    var filterAccount = [];
    var checkValues = /^[0-9]+$/;
    this.accounts.forEach(function (type) {
      if (type.n.match(checkValues)) {
        filterAccount.push(type);
      }
    });
    console.log("2asdasdas", filterAccount);
    let { currentPage, allPerPage } = this.state;
    let indexOfLastTodo = currentPage * allPerPage;
    let indexOfFirstTodo = indexOfLastTodo - allPerPage;
    prev = currentPage > 0 ? currentPage - 1 : 0;
    last = Math.ceil(filterAccount && filterAccount.length / allPerPage);
    next = last === currentPage ? currentPage : currentPage + 1;

    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }
    return (
      <div>
        <div
          style={{
            backgroundColor: "#5cba49",
            height: "66px",
            marginBottom: "2%",
          }}
        >
          <img
            alt="logo"
            style={{ height: "66px", marginLeft: "5%" }}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-fURXJzStp3hE6Z36vxaGHpdPR2uscmuHBm1YbvqqWUjGBzbRH80SG_Uk4EYVl1hL6Kw&usqp=CAU"
          ></img>
        </div>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <h6>Consulta de saldo</h6>
        </div>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <h5>Selecciona la cuenta a Consultar</h5>
        </div>
        <div
          style={{ display: "flex", justifyContent: "center" }}
          className="row  row-cols-md-3 row-cols-lg-4"
        >
          {prev === 0 ? (
            ""
          ) : (
            <button
            className="acc_card"
              style={{
                backgroundColor: "#5cba49",
                width: "10%",
                height: "100px",
                marginTop: "2%",
                marginLeft: "1.5%",
                borderColor: "transparent",
              }}
            >
              <a
                style={{ color: "white" }}
                onClick={this.handleClick}
                id={prev}
                href={prev}
              >
                {"<<"} Opciones anteriores
              </a>
            </button>
          )}

          {filterAccount &&
            filterAccount
              .slice(indexOfFirstTodo, indexOfLastTodo)
              .map((p, key) => (
                <button
                className="acc_btn"
                  onClick={() => (window.location.href = `/detalles/${p.n}`)}
                  key={key}
                  style={{
                    backgroundColor: "#5cba49",
                    width: "10%",
                    height: "100px",
                    marginTop: "2%",
                    marginLeft: "1.5%",
                    borderRadius: "4%",
                    borderColor: "transparent",
                  }}
                >
                  <h5   className="acc_card" style={{ color: "white" }}>
                    {this.typeAccount(p.tipo_letras)}
                  </h5>

                  <h5   className="acc_card" style={{ color: "white" }}>Nro: {p.n && p.n}</h5>
                </button>
              ))}
          <br />
          {currentPage === last ? (
            ""
          ) : (
            <button
            className="acc_card"
              style={{
                backgroundColor: "#5cba49",
                width: "10%",
                height: "100px",
                marginTop: "2%",
                marginLeft: "1.5%",
                borderRadius: "4%",
                borderColor: "transparent",
              }}
            >
              <a
                style={{ color: "white" }}
                onClick={this.handleClick}
                id={pageNumbers[currentPage]}
                href={pageNumbers[currentPage]}
              >
                Más Opciones {">>"}
              </a>
            </button>
          )}
        </div>
        <button
          style={{
            marginTop: "2%",
            backgroundColor: "#5cba49",
            color: "white",
            borderRadius: "20%",
            marginLeft: "20px",
            borderColor: "transparent",
          }}
          onClick={() => this.LogOut()}
        >
          Salir
        </button>
      </div>
    );
  }
}

export default Main;
