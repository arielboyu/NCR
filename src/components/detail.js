import React, { Component } from "react";
import Cookies from "universal-cookie";

const cookies = new Cookies();

class Detail extends Component {
  goDetail() {
    window.location.href = "./detail";
  }

  typeAccount(type) {
    if (type === "CC") {
      return "Cuenta Corriente";
    } else if (type === "CA") {
      return "Caja De Ahorro";
    } else if (type === "CCP") {
      return "Caja De Ahorro en Pesos ";
    }
  }

  render() {
    let id = this.props.match.params.id;
    let accounts = cookies.get("accounts");
    let match = [];
    for (let i = 0; i < accounts.length; i++) {
      if (accounts[i].n === id) {
        match = accounts[i];
      }
    }
    // console.log("idnum:", match);

    return (
      <>
        <div style={{ backgroundColor: "#5cba49", height: "66px" }}>
          <img
            alt="logo"
            style={{ height: "66px", marginLeft: "5%" }}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-fURXJzStp3hE6Z36vxaGHpdPR2uscmuHBm1YbvqqWUjGBzbRH80SG_Uk4EYVl1hL6Kw&usqp=CAU"
          ></img>
        </div>
        <h6
          style={{
            marginTop: "2%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Consulta de saldo
        </h6>
        <div
          style={{
            marginTop: "2%",
            marginBottom: "5%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <h2>Este es tu saldo Actual</h2>
        </div>
        <div
          style={{ marginTop: "2%", display: "flex", justifyContent: "center" }}
        >
          {" "}
          <ul style={{ listStyle: "none" }}>
            <li>Saldo de la cuenta: {match.saldo}</li>
            <li style={{ marginTop: "6%" }}>
              Tipo de cuenta: {this.typeAccount(match.tipo_letras)}{" "}
            </li>
            <li style={{ marginTop: "6%" }}>Numero de cuenta: {match.n} </li>
          </ul>
        </div>
        <button
          style={{
            marginTop: "4%",
            backgroundColor: "#5cba49",
            color: "white",
            borderRadius: "20%",
            marginLeft: "20px",
            borderColor: "transparent",
          }}
          onClick={() => window.history.back()}
        >
          Salir
        </button>
      </>
    );
  }
}

export default Detail;
