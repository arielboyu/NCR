import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import Cookies from "universal-cookie";

const baseUrl = "http://localhost:3001/usuarios";
const cookies = new Cookies();

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        username: "",
        clave: "",
      },
    };
  }

  handleChange = async (e) => {
    await this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  Login = async () => {
    await axios
      .get(baseUrl, {
        params: {
          username: this.state.form.username,
          clave: this.state.form.clave,
        },
      })
      .then((response) => {
        return response.data;
      })
      .then((response) => {
        if (response.length > 0) {
          var respuesta = response[0];
          console.log(respuesta);
          cookies.set("idnum", respuesta.idnum, { path: "/" });
          cookies.set("username", respuesta.username, { path: "/" });
          cookies.set("accounts", respuesta.accounts, { path: "/" });
          window.location.href = "./menu";
        } else {
          alert("El usuario o la contraseña no son correctos");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  componentDidMount() {
    if (cookies.get("username")) {
      window.location.href = "./menu";
    }
  }

  render() {
    // console.log('accounts: '+cookies.get('accounts'));
    return (
      <>
        <div
          style={{
            backgroundColor: "#5cba49",
            height: "66px",
            marginBottom: "2%",
          }}
        >
          <img
          alt="logo"
            style={{ height: "66px", marginLeft: "5%" }}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-fURXJzStp3hE6Z36vxaGHpdPR2uscmuHBm1YbvqqWUjGBzbRH80SG_Uk4EYVl1hL6Kw&usqp=CAU"
          ></img>
        </div>
        <div
          style={{
            position: "absolute",
            left: "50%",
            top: "50%",
            transform: "translate(-50%, -50%)",
            border: "1px solid #5cba49",
            padding: "40px",
            backgroundColor: "white",
          }}
        >
          <div style={{ textAlign: "center" }}>
            <div className="form-group">
              <label>Usuario: </label>
              <br />
              <input
                type="text"
                className="form-control"
                name="username"
                onChange={this.handleChange}
              />
              <br />
              <label>Clave: </label>
              <br />
              <input
                type="clave"
                className="form-control"
                name="clave"
                onChange={this.handleChange}
              />
              <br />
              <button
                style={{
                  backgroundColor: "#5cba49",
                  borderRadius: "20px",
                  opacity: "0.8",
                }}
                onClick={() => this.Login()}
              >
                Ingresar
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Login;
